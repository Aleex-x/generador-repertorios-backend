const isNumber = (n) => {
  // eslint-disable-next-line no-restricted-globals
  return !isNaN(n);
};

/* eslint-disable arrow-body-style */
const path = (paths, obj) => {
  return paths.reduce((val, key) => ((val && val[key] !== 'undefined') ? val[key] : undefined), obj);
};

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index += 1) {
    // eslint-disable-next-line no-await-in-loop
    await callback(array[index], index, array);
  }
};

module.exports = {
  isNumber,
  path,
  asyncForEach
};
