const mongoose = require('mongoose');
import logger from './libs/logger';

const {
  NOTES_APP_MONGODB_HOST,
  NOTES_APP_MONGODB_PORT,
  NOTES_APP_MONGODB_DB,
  NOTES_APP_MONGODB_USER,
  NOTES_APP_MONGODB_PW
} = process.env;

const MONGODB_URI = `mongodb://${NOTES_APP_MONGODB_USER}:${NOTES_APP_MONGODB_PW}@${NOTES_APP_MONGODB_HOST}:${NOTES_APP_MONGODB_PORT}/${NOTES_APP_MONGODB_DB}?authSource=admin`;

mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => logger.error('DB es connected'))
  .catch(err => logger.error(err.stack));
