import { Router } from 'express';
const router = Router();

import * as songsCtrl from '../controllers/songs.controller';

router.get('/', songsCtrl.getSongs);
router.put('/', songsCtrl.insertSong);
router.put('/:idSong', songsCtrl.updateSongById);
router.delete('/:idSong', songsCtrl.deleteSongById);
router.post('/', songsCtrl.getSongsByFilters);

export default router;
