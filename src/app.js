require('dotenv').config();
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import pkg from '../package.json';
import cors from 'cors';

// RUTAS!!
import songRoutes from './routes/songs.routes';

const app = express();

// colocar un nombre a una variable
app.set('pkg', pkg);

// este man se usa para checar los request que llegan al server
app.use(morgan('dev'));
app.use(helmet());
app.use(cors());
app.use(express.json());

app.get('/', (req, res) => {
  res.json({
    name: app.get('pkg').name,
    author: app.get('pkg').author,
    description: app.get('pkg').description,
    version: app.get('pkg').version
  });
});

app.use('/api/songs', songRoutes);

export default app;
