import { Schema, model } from 'mongoose';

const songSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  artist: {
    type: String,
    required: true
  },
  thematic: {
    type: Array,
    required: true
  },
  mood: {
    type: Array,
    required: true
  },
  spanish: Boolean
}, {
  timestamps: true,
  versionKey: false
});

export default model('Song', songSchema);
