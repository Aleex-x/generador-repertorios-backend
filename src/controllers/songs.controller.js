import Song from '../models/Song';
import logger from '../libs/logger';

const getRandom = (arr, n) => {
  let result = new Array(n);
  let len = arr.length;
  let taken = new Array(len);
  if (n > len) {
    throw new RangeError('getRandom: more elements taken than available');
  }

  while (n--) {
    const x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    // eslint-disable-next-line no-plusplus
    taken[x] = --len in taken ? taken[len] : len;
  }

  return result;
};

const getSongsArray = (arr, mood, selected, n) => {
  let arrNumber = [];
  // get the array filtered by mood
  let fArray = arr.filter((song) => song.mood.indexOf(mood) !== -1);
  // remove same songs included in past set
  fArray = fArray.filter((el) => !selected.includes(el));
  // get 'n' random elements from filtered array
  if (fArray.length >= n) arrNumber = getRandom(fArray, n);
  return arrNumber;
};

const createMatchedSet = (english, spanish, past, nSongs, pair) => {
  let c = [];
  let fSpanish = spanish;
  let fEnglish = english;

  past.forEach((set) => {
    fSpanish = fSpanish.filter((el) => !set.includes(el));
  });
  past.forEach((set) => {
    fEnglish = fEnglish.filter((el) => !set.includes(el));
  });

  const twoStarters = getSongsArray(fSpanish, 'starter', [], 2);
  const followersInSpanish = getSongsArray(fSpanish, 'follower', twoStarters, (pair ? nSongs - 2 : nSongs - 1));
  const followersInEnglish = getSongsArray(fEnglish, 'follower', [], (nSongs - 2));
  const twoFinishers = getSongsArray(fEnglish, 'finisher', followersInEnglish, 2);

  c = [...twoStarters, ...followersInSpanish, ...followersInEnglish, ...twoFinishers];
  return c;
};

const createSets = (full, past, nSongs) => {
  let c = [];
  let filtered = full;

  past.forEach((set) => {
    filtered = filtered.filter((el) => !set.includes(el));
  });

  const twoStarters = getSongsArray(filtered, 'starter', [], 2);
  const followers = getSongsArray(filtered, 'follower', twoStarters, (nSongs - 4));
  const twoFinishers = getSongsArray(filtered, 'finisher', followers, 2);

  c = [...twoStarters, ...followers, ...twoFinishers];

  return c;
};

const filterSongs = (songs, songsPerSet, setsNumber, both) => {
  let allSets = [];
  let set;
  let spanishArray = [];
  let englishArray = [];

  if (both) {
    spanishArray = songs.filter((song) => song.spanish);
    englishArray = songs.filter((song) => !song.spanish);
  }

  for (let i = 0; i < setsNumber; i += 1) {
    set = [];

    if (both) {
      set = createMatchedSet(englishArray, spanishArray, allSets,
        parseInt(songsPerSet / 2, 10), (songsPerSet % 2 === 0));
    } else {
      set = createSets(songs, allSets, songsPerSet);
    }

    allSets = [...allSets, set];
  }

  return allSets;
};

export const getSongsByFilters = async (req, res) => {
  try {
    const {
      thematic,
      setsType,
      songsPerSet,
      setsNumber
    } = req.body;

    let songs = [];

    if (setsType === 'both') {
      songs = await Song.find({ thematic: { $in: [thematic] } });
    } else if (setsType === 'spanish') {
      songs = await Song.find({ thematic: { $in: [thematic] }, spanish: true });
    } else {
      songs = await Song.find({ thematic: { $in: [thematic] }, spanish: false });
    }

    const songsFiltered = filterSongs(songs, parseInt(songsPerSet, 10), setsNumber, setsType === 'both');

    if (songsFiltered[0].length < parseInt(songsPerSet, 10)) {
      res.status(200).json({
        code: 'failed',
        message: 'No hay suficientes canciones del tipo seleccionado',
        data: []
      });
    } else {
      res.status(200).json({
        code: 'success',
        message: '',
        data: songsFiltered
      });
    }
  } catch (e) {
    logger.error(e.stack);
    res.status(401).json({ code: 'error', message: 'ocurrió un error al procesar el set' });
  }
};

export const getSongs = async (req, res) => {
  try {
    const songs = await Song.find();
    res.json(songs);
  } catch (e) {
    logger.error(e.stack);
  }
};

export const insertSong = async (req, res) => {
  const newSong = new Song(req.body);

  try {
    const songSaved = await newSong.save();
    res.status(201).json({ code: 'success', message: `Canción "${songSaved.name}" guardada con éxito` });
  } catch (e) {
    logger.error(e.stack);
  }
};

export const updateSongById = async (req, res) => {
  try {
    const updatedProduct = await Song.findByIdAndUpdate(req.params.idSong, req.body, {
      new: true
    });

    res.status(200).json(updatedProduct);
  } catch (e) {
    logger.error(e.stack);
  }
};

export const deleteSongById = async (req, res) => {
  const {
    idSong
  } = req.params;

  try {
    await Song.findByIdAndDelete(idSong);
    res.status(204).json();
  } catch (e) {
    logger.error(e.stack);
    res.status(400).json();
  }
};
