import app from './app';
import './database';
const logger = require('./libs/logger');

app.listen(4000);

logger.info('Server running in port 4000');
